# GitLab i18n Ru

This project is made by an independent team to improve GitLab in Russian.
Our goal is to make GitLab in Russian convenient and clear to everyone.

If you have a question or a suggestion, let's discuss it: https://gitlab.com/gitlab-i18n-ru/gitlab-i18n-ru/-/issues

----

Этот проект создан независимой командой для улучшения качества русской локализации GitLab.
Наша цель состоит в том, чтобы сделать работу с русскоязычным интерфейсом удобной
и понятной для всех.

Если у вас вопрос или предложение, давайте обсудим: https://gitlab.com/gitlab-i18n-ru/gitlab-i18n-ru/-/issues